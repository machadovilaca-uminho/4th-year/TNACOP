package market;

import market.controllers.OrderController;
import market.models.Product;
import market.repositories.ClientRepository;
import market.repositories.OrderRepository;
import market.repositories.ProductRepository;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.*;

public class OrderTest {
    private final ClientRepository clientRepository = new ClientRepository();
    private final OrderRepository orderRepository = new OrderRepository();
    private final ProductRepository productRepository = new ProductRepository();

    @Test
    public void testFulfilOrder() {
        long clientId = clientRepository.save("User1");
        long orderId = orderRepository.save(clientId);
        long productId1 = productRepository.save("Product 1", 1, 1);
        long productId2 = productRepository.save("Product 2", 1, 100);


        orderRepository.addProduct(orderId, productId1);
        orderRepository.addProduct(orderId, productId2);

        boolean done = orderRepository.finish(orderId);

        assertTrue(done);

        Product p1 = productRepository.show(productId1);
        assertEquals(0, p1.getStock());

        Product p2 = productRepository.show(productId2);
        assertEquals(99, p2.getStock());
    }

    @Test
    public void testCantFulfilOrder() {
        long clientId = clientRepository.save("User2");
        long orderId = orderRepository.save(clientId);
        long productId3 = productRepository.save("Product 3", 1, 0);
        long productId4 = productRepository.save("Product 4", 1, 3);

        orderRepository.addProduct(orderId, productId3);
        orderRepository.addProduct(orderId, productId4);

        boolean done = orderRepository.finish(orderId);

        assertFalse(done);

        Product p4 = productRepository.show(productId4);
        assertEquals(3, p4.getStock());
    }
}
