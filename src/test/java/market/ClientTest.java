package market;

import market.models.Client;
import market.repositories.ClientRepository;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ClientTest {
    private final ClientRepository clientRepository = new ClientRepository();

    @Test
    public void testSaveClient() {
        long id1 = clientRepository.save("User1");
        long id2 = clientRepository.save("User2");

        assertTrue(id1 >= 0 && id2 >= 0);
    }

    @Test
    public void testShowClient() {
        long id3 = clientRepository.save("User3");

        Client client = clientRepository.show(id3);

        assertEquals(client.getName(), "User3");
    }

    @Test
    public void testIndexClients() {
        long id4 = clientRepository.save("User4");

        if (clientRepository.get().stream().noneMatch(p -> p.getId() == id4)) {
            fail();
        }
    }
}
