package shop;

import org.springframework.shell.Bootstrap;
import shop.models.*;
import shop.models.impls.*;

import java.io.IOException;
import java.util.List;

public class Shop {
    public static void main(String[] args) throws IOException {
        Bootstrap.main(args);
    }

    public static void sa(String[] args) {
        Client client = new ClientImpl("Márcia");
        System.out.println(client.getName());

        Clients clients = new ClientsImpl();
        clients.index().forEach(System.out::println);

        Products products = new ProductsImpl();
        List<Product> ps = products.index();
        ps.forEach(System.out::println);

        Order order = new OrderImpl(client);

        order.addProduct(ps.get(0));
        order.addProduct(ps.get(1));
        order.removeProduct(ps.get(0));

        Orders orders = new OrdersImpl();
        orders.index().forEach(System.out::println);

        order.confirm();

        orders.index().forEach(System.out::println);
    }
}
