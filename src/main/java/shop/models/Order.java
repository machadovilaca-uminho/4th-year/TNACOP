package shop.models;

import java.util.List;

public interface Order {
    long getId();

    boolean isCompleted();

    Client getClient();

    List<Product> getProducts();

    void addProduct(Product product);

    void removeProduct(Product product);

    void confirm();

    String toString();
}
