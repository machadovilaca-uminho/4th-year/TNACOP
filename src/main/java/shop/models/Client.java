package shop.models;

public interface Client {
    long getId();

    String getName();

    String toString();
}
