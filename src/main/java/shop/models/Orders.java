package shop.models;

import java.util.List;

public interface Orders {
    List<Order> index();

    Order show(long orderId);
}
