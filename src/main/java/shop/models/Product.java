package shop.models;

public interface Product {
    long getId();

    String getName();

    int getPrice();

    int getStock();

    String toString();
}
