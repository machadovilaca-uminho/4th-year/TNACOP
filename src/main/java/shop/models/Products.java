package shop.models;

import java.util.List;

public interface Products {
    List<Product> index();

    Product show(long productId);
}
