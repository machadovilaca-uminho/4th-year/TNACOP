package shop.models;

import java.util.List;

public interface Clients {
    List<Client> index();

    Client show(long clientId);
}
