package shop.models.impls;

import org.apache.commons.lang3.SerializationUtils;
import shop.connection.Connection;
import shop.models.Product;
import shop.models.Products;

import java.util.ArrayList;
import java.util.List;


public class ProductsImpl implements Products {
    public List<Product> index() {
        byte[] data = Connection.sendMessage("product;index");
        market.models.Product[] cs = SerializationUtils.deserialize(data);

        List<Product> products = new ArrayList<>();

        for (market.models.Product product : cs) {
            products.add(new ProductImpl(product.getId(), product.getName(), product.getPrice(), product.getStock()));
        }

        return products;
    }

    public Product show(long productId) {
        byte[] data = Connection.sendMessage("product;show-" + productId);

        market.models.Product product = SerializationUtils.deserialize(data);

        return new ProductImpl(product.getId(), product.getName(), product.getPrice(), product.getStock());
    }
}
