package shop.models.impls;

import helpers.Colors;
import org.apache.commons.lang3.SerializationUtils;
import shop.connection.Connection;
import shop.models.Client;


public class ClientImpl implements Client {
    private final long id;
    private final String name;

    public ClientImpl(String name) {
        byte[] data = Connection.sendMessage("client;create-" + name);
        this.id = SerializationUtils.deserialize(data);
        this.name = name;
    }

    ClientImpl(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return Colors.YELLOW +
                "   ClientImpl:" + '\n' +
                Colors.RESET +
                Colors.BLUE +
                "       -> Id: " +
                Colors.RESET +
                id + '\n' +
                Colors.BLUE +
                "       -> Name: " +
                Colors.RESET +
                name + '\n';
    }
}
