package shop.models.impls;

import helpers.Colors;
import org.apache.commons.lang3.SerializationUtils;
import shop.connection.Connection;
import shop.models.Client;
import shop.models.Order;
import shop.models.Product;

import java.util.ArrayList;
import java.util.List;


public class OrderImpl implements Order {
    private final long id;
    private final Client client;
    private final List<Product> products;
    private boolean completed;

    public OrderImpl(Client client) {
        byte[] data = Connection.sendMessage("order;create-" + client.getId());

        market.models.Order order = SerializationUtils.deserialize(data);

        this.id = order.getId();
        this.completed = order.getCompleted();
        this.client = new ClientImpl(order.getClient().getId(), order.getClient().getName());

        this.products = new ArrayList<>();

        order.getProducts().forEach(p -> products.add(new ProductImpl(
                p.getId(),
                p.getName(),
                p.getPrice(),
                p.getStock()
        )));
    }

    OrderImpl(long id, boolean completed, Client client, List<Product> products) {
        this.id = id;
        this.completed = completed;
        this.client = client;
        this.products = products;
    }

    public long getId() {
        return id;
    }

    public boolean isCompleted() {
        return completed;
    }

    public Client getClient() {
        return client;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void addProduct(Product product) {
        byte[] data = Connection.sendMessage("order;addProduct-" + id + "-" + product.getId());

        market.models.Order order = SerializationUtils.deserialize(data);

        this.products.clear();

        order.getProducts().forEach(p -> products.add(new ProductImpl(
                p.getId(),
                p.getName(),
                p.getPrice(),
                p.getStock()
        )));
    }

    public void removeProduct(Product product) {
        byte[] data = Connection.sendMessage("order;removeProduct-" + id + "-" + product.getId());

        market.models.Order order = SerializationUtils.deserialize(data);

        this.products.clear();

        order.getProducts().forEach(p -> products.add(new ProductImpl(
                p.getId(),
                p.getName(),
                p.getPrice(),
                p.getStock()
        )));
    }

    public void confirm() {
        byte[] data = Connection.sendMessage("order;confirm-" + id);

        market.models.Order order = SerializationUtils.deserialize(data);

        this.completed = order.getCompleted();
    }

    public String toString() {
        return Colors.CYAN +
                "OrderImpl: \n" +
                Colors.RESET +
                Colors.YELLOW +
                "   -> Id: " +
                Colors.RESET +
                id + '\n' +
                Colors.YELLOW +
                "   -> Completed: " +
                Colors.RESET +
                completed + '\n' +
                Colors.YELLOW +
                "   -> Client:\n" +
                Colors.RESET +
                client + '\n' +
                Colors.YELLOW +
                "   -> Products:\n" +
                Colors.RESET +
                products + '\n';
    }
}
