package shop.models.impls;

import helpers.Colors;
import shop.models.Product;


public class ProductImpl implements Product {
    private final long id;
    private final String name;
    private final int price;
    private final int stock;

    ProductImpl(long id, String name, int price, int stock) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.stock = stock;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getStock() {
        return stock;
    }

    public String toString() {
        return Colors.YELLOW +
                "   ProductImpl:\n" +
                Colors.RESET +
                Colors.BLUE +
                "       -> Id: " +
                Colors.RESET +
                id + '\n' +
                Colors.BLUE +
                "       -> Name: " +
                Colors.RESET +
                name + '\n' +
                Colors.BLUE +
                "       -> Price: " +
                Colors.RESET +
                price + '\n' +
                Colors.BLUE +
                "       -> Stock: " +
                Colors.RESET +
                stock + '\n';
    }
}
