package shop.models.impls;

import org.apache.commons.lang3.SerializationUtils;
import shop.connection.Connection;
import shop.models.Client;
import shop.models.Clients;

import java.util.ArrayList;
import java.util.List;


public class ClientsImpl implements Clients {
    public List<Client> index() {
        byte[] data = Connection.sendMessage("client;index");
        market.models.Client[] cs = SerializationUtils.deserialize(data);

        List<Client> clients = new ArrayList<>();

        for (market.models.Client c : cs) {
            clients.add(new ClientImpl(c.getId(), c.getName()));
        }

        return clients;
    }

    public Client show(long clientId) {
        byte[] data = Connection.sendMessage("client;show-" + clientId);

        market.models.Client client = SerializationUtils.deserialize(data);

        return new ClientImpl(client.getId(), client.getName());
    }
}
