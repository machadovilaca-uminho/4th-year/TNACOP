package shop.models.impls;

import org.apache.commons.lang3.SerializationUtils;
import shop.connection.Connection;
import shop.models.Order;
import shop.models.Orders;
import shop.models.Product;

import java.util.ArrayList;
import java.util.List;


public class OrdersImpl implements Orders {
    public List<Order> index() {
        byte[] data = Connection.sendMessage("order;index");
        market.models.Order[] cs = SerializationUtils.deserialize(data);

        List<Order> orders = new ArrayList<>();

        for (market.models.Order order : cs) {
            orders.add(orderFromMarket(order));
        }

        return orders;
    }

    public Order show(long clientId) {
        byte[] data = Connection.sendMessage("order;show-" + clientId);

        market.models.Order order = SerializationUtils.deserialize(data);

        return orderFromMarket(order);
    }

    private Order orderFromMarket(market.models.Order order) {
        List<Product> products = new ArrayList<>();

        order.getProducts().forEach(p -> products.add(new ProductImpl(
                p.getId(),
                p.getName(),
                p.getPrice(),
                p.getStock()
        )));

        return new OrderImpl(
                order.getId(),
                order.getCompleted(),
                new ClientImpl(order.getClient().getId(), order.getClient().getName()),
                products
        );
    }
}
