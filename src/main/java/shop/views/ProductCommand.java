package shop.views;

import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import shop.models.Product;
import shop.models.Products;
import shop.models.impls.ProductsImpl;

import java.util.List;

@Component
public class ProductCommand implements CommandMarker {
    @CliCommand(value = {"ip"}, help = "Index all products")
    public List<Product> indexProducts() {
        Products products = new ProductsImpl();
        return products.index();
    }

    @CliCommand(value = {"sp"}, help = "Show a product by id")
    public Product showProduct(@CliOption(key = {"", "id"}) long id) {
        Products products = new ProductsImpl();
        return products.show(id);
    }
}
