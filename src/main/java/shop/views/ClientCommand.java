package shop.views;

import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import shop.models.Client;
import shop.models.Clients;
import shop.models.impls.ClientImpl;
import shop.models.impls.ClientsImpl;

import java.util.List;

@Component
public class ClientCommand implements CommandMarker {
    @CliCommand(value = {"ic"}, help = "Index all clients")
    public List<Client> indexClients() {
        Clients clients = new ClientsImpl();
        return clients.index();
    }

    @CliCommand(value = {"sc"}, help = "Show a client by id")
    public Client showClient(@CliOption(key = {"", "id"}) long id) {
        Clients clients = new ClientsImpl();
        return clients.show(id);
    }

    @CliCommand(value = {"cc"}, help = "Create a client with name (--name)")
    public Client createClient(@CliOption(key = {"", "name"}) String name) {
        return new ClientImpl(name);
    }
}
