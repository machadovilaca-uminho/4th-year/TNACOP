package shop.views;

import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import shop.models.*;
import shop.models.impls.ClientsImpl;
import shop.models.impls.OrderImpl;
import shop.models.impls.OrdersImpl;
import shop.models.impls.ProductsImpl;

import java.util.List;

@Component
public class OrderCommand implements CommandMarker {
    @CliCommand(value = {"io"}, help = "Index all orders")
    public List<Order> indexOrders() {
        Orders orders = new OrdersImpl();
        return orders.index();
    }

    @CliCommand(value = {"so"}, help = "Show an order by id")
    public Order showOrder(@CliOption(key = {"", "id"}) long id) {
        Orders orders = new OrdersImpl();
        return orders.show(id);
    }

    @CliCommand(value = {"co"}, help = "Create an order to a client (--clientId)")
    public Order createClient(@CliOption(key = {"", "clientId"}) long clientId) {
        Clients clients = new ClientsImpl();
        Client client = clients.show(clientId);

        return new OrderImpl(client);
    }

    @CliCommand(value = {"ap"}, help = "Add a product to an order (--orderId, --productId)")
    public Order addProduct(@CliOption(key = {"", "orderId"}) long orderId, @CliOption(key = {"", "productId"}) long productId) {
        Order order = showOrder(orderId);
        Products products = new ProductsImpl();
        Product product = products.show(productId);
        order.addProduct(product);

        return order;
    }

    @CliCommand(value = {"rp"}, help = "Remove from product from an order (--orderId, --productId)")
    public Order removeProduct(@CliOption(key = {"", "orderId"}) long orderId, @CliOption(key = {"", "productId"}) long productId) {
        Order order = showOrder(orderId);
        Products products = new ProductsImpl();
        Product product = products.show(productId);
        order.removeProduct(product);

        return order;
    }

    @CliCommand(value = {"cfo"}, help = "Confirm an order by id")
    public Order confirm(@CliOption(key = {"", "id"}) long id) {
        Order order = showOrder(id);
        order.confirm();

        return order;
    }
}
