package shop.connection;

import java.util.Arrays;
import java.util.List;

public class Connection {
    static Server preferredServer = null;

    static final List<Server> servers = Arrays.asList(
            new Server("localhost", 8000),
            new Server("localhost", 8001),
            new Server("localhost", 8002)
    );

    public static byte[] sendMessage(String message) {
        if (preferredServer == null) {
            return sendAll(message);
        }

        return sendOne(message, preferredServer);
    }

    private static byte[] sendAll(String message) {
        for (Server server : servers) {
            Connector connector = new Connector(server, message);
            byte[] data = connector.start();

            if (data != null) {
                preferredServer = server;
                return data;
            }
        }

        return null;
    }

    private static byte[] sendOne(String message, Server server) {
        Connector connector = new Connector(server, message);
        byte[] data = connector.start();

        if (data != null) {
            preferredServer = server;
            return data;
        }

        preferredServer = null;
        return sendMessage(message);
    }
}
