package shop.connection;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class Connector {
    private final Server server;
    private final String message;

    public Connector(Server server, String message) {
        super();

        this.server = server;
        this.message = message;
    }

    public byte[] start() {
        try {
            Socket socket = new Socket(server.getHost(), server.getPort());
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            out.println(message);
            out.flush();
            return socket.getInputStream().readAllBytes();
        } catch (IOException ignored) {
        }

        return null;
    }
}
