package market.controllers;

import market.models.Client;
import market.repositories.ClientRepository;
import market.server.Server;
import org.apache.commons.lang3.SerializationUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class ClientController {
    private final ClientRepository clientRepository = new ClientRepository();

    public void handle(String action) {
        try {
            handler(action);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handler(String action) throws IOException {
        String[] parts = action.split(";");

        int connectionHash = Integer.parseInt(parts[0]);
        Socket socket = Server.connectionOutput.get(connectionHash);
        OutputStream out = socket == null ? null : socket.getOutputStream();

        String[] components = parts[2].split("-");

        switch (components[0]) {
            case "index" -> index(out);
            case "create" -> create(out, components);
            case "show" -> show(out, components);
        }

        if (socket != null) {
            out.close();
            Server.connectionOutput.remove(socket.hashCode());
        }
    }

    private void index(OutputStream out) throws IOException {
        if (out != null) {
            Client[] clients = clientRepository.get().toArray(Client[]::new);
            out.write(SerializationUtils.serialize(clients));
            out.flush();
        }
    }

    private void create(OutputStream out, String[] components) throws IOException {
        long id = clientRepository.save(components[1]);

        if (out != null) {
            out.write(SerializationUtils.serialize(id));
            out.flush();
        }
    }

    private void show(OutputStream out, String[] components) throws IOException {
        if (out != null) {
            long orderId = Long.parseLong(components[1]);

            Client client = clientRepository.show(orderId);
            out.write(SerializationUtils.serialize(client));
            out.flush();
        }
    }
}
