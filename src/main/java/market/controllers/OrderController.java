package market.controllers;

import market.models.Order;
import market.repositories.OrderRepository;
import market.server.Server;
import org.apache.commons.lang3.SerializationUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class OrderController {
    private final OrderRepository orderRepository = new OrderRepository();

    public void handle(String action) {
        try {
            handler(action);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handler(String action) throws IOException {
        String[] parts = action.split(";");

        int connectionHash = Integer.parseInt(parts[0]);
        Socket socket = Server.connectionOutput.get(connectionHash);
        OutputStream out = socket == null ? null : socket.getOutputStream();

        String[] components = parts[2].split("-");

        switch (components[0]) {
            case "index" -> index(out);
            case "create" -> create(out, components);
            case "show" -> show(out, components);
            case "addProduct" -> addProduct(out, components);
            case "removeProduct" -> removeProduct(out, components);
            case "confirm" -> confirm(out, components);
        }

        if (socket != null) {
            out.close();
            Server.connectionOutput.remove(socket.hashCode());
        }
    }

    private void index(OutputStream out) throws IOException {
        if (out != null) {
            Order[] orders = orderRepository.get().toArray(Order[]::new);
            out.write(SerializationUtils.serialize(orders));
            out.flush();
        }
    }

    private void create(OutputStream out, String[] components) throws IOException {
        long clientId = Long.parseLong(components[1]);
        long orderId = orderRepository.save(clientId);

        if (out != null) {
            Order order = orderRepository.show(orderId);
            out.write(SerializationUtils.serialize(order));
            out.flush();
        }
    }

    private void show(OutputStream out, String[] components) throws IOException {
        long orderId = Long.parseLong(components[1]);

        if (out != null) {
            Order order = orderRepository.show(orderId);
            out.write(SerializationUtils.serialize(order));
            out.flush();
        }
    }

    private void addProduct(OutputStream out, String[] components) throws IOException {
        long orderId = Long.parseLong(components[1]);
        long productId = Long.parseLong(components[2]);

        orderRepository.addProduct(orderId, productId);

        if (out != null) {
            Order order = orderRepository.show(orderId);
            out.write(SerializationUtils.serialize(order));
            out.flush();
        }
    }

    private void removeProduct(OutputStream out, String[] components) throws IOException {
        long orderId = Long.parseLong(components[1]);
        long productId = Long.parseLong(components[2]);

        orderRepository.removeProduct(orderId, productId);

        if (out != null) {
            Order order = orderRepository.show(orderId);
            out.write(SerializationUtils.serialize(order));
            out.flush();
        }
    }

    private void confirm(OutputStream out, String[] components) throws IOException {
        long orderId = Long.parseLong(components[1]);

        orderRepository.finish(orderId);

        if (out != null) {
            Order order = orderRepository.show(orderId);
            out.write(SerializationUtils.serialize(order));
            out.flush();
        }
    }
}
