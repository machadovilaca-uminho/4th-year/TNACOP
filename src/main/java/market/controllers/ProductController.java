package market.controllers;

import market.models.Product;
import market.repositories.ProductRepository;
import market.server.Server;
import org.apache.commons.lang3.SerializationUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class ProductController {
    private final ProductRepository productRepository = new ProductRepository();

    public void handle(String action) {
        try {
            handler(action);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handler(String action) throws IOException {
        String[] parts = action.split(";");

        int connectionHash = Integer.parseInt(parts[0]);
        Socket socket = Server.connectionOutput.get(connectionHash);
        OutputStream out = socket == null ? null : socket.getOutputStream();

        String[] components = parts[2].split("-");

        switch (components[0]) {
            case "index" -> index(out);
            case "show" -> show(out, components);
        }

        if (socket != null) {
            out.close();
            Server.connectionOutput.remove(socket.hashCode());
        }
    }

    private void index(OutputStream out) throws IOException {
        if (out != null) {
            Product[] products = productRepository.get().toArray(Product[]::new);

            out.write(SerializationUtils.serialize(products));
            out.flush();
        }
    }

    private void show(OutputStream out, String[] components) throws IOException {
        if (out != null) {
            long productId = Long.parseLong(components[1]);

            Product product = productRepository.show(productId);

            out.write(SerializationUtils.serialize(product));
            out.flush();
        }
    }
}
