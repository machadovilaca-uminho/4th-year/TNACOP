package market;

import market.server.InputParserImpl;
import market.replication.InputParser;
import market.replication.Replicator;
import market.repositories.ProductRepository;

public class Market {
    public static InputParser inputParser;

    public static void main(String[] args) throws Exception {
        ProductRepository productRepository = new ProductRepository();

        for (int i = 1; i <= 20; i++) {
            int stock = i % 5;

            productRepository.save("Product " + i, i, stock);
        }

        Replicator replicator = new Replicator();
        inputParser = new InputParserImpl(replicator);
        replicator.start(args[0]);

        while (true) ;
    }
}
