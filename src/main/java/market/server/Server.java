package market.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Server extends Thread {
    public static final Map<Integer, Socket> connectionOutput = new ConcurrentHashMap<>();
    private ServerSocket serverSocket;

    public Server(String serverId) {
        try {
            this.serverSocket = new ServerSocket(8000 + Integer.parseInt(serverId));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(110);
        }
    }

    public void run() {
        while (true) {
            try {
                Socket socket = serverSocket.accept();

                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                connectionOutput.put(socket.hashCode(), socket);

                ConnectionHandler connectionHandler = new ConnectionHandler(socket.hashCode(), in);
                connectionHandler.start();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(120);
            }
        }
    }
}
