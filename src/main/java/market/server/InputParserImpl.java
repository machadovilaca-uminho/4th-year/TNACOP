package market.server;

import market.controllers.ClientController;
import market.controllers.OrderController;
import market.controllers.ProductController;
import market.replication.InputParser;
import market.replication.Replicator;

import java.util.regex.Pattern;

public class InputParserImpl extends InputParser {
    private final ClientController clientController = new ClientController();
    private final OrderController orderController = new OrderController();
    private final ProductController productController = new ProductController();

    private final Pattern clientMessage = Pattern.compile("\\d+;client;.*");
    private final Pattern orderMessage = Pattern.compile("\\d+;order;.*");
    private final Pattern productMessage = Pattern.compile("\\d+;product;.*");

    public InputParserImpl(Replicator replicator) {
        super(replicator);
    }

    public void handleMessage(String message) {
        System.out.println("Spread Message: " + message);

        if (clientMessage.matcher(message).matches()) {
            clientController.handle(message);
        } else if (orderMessage.matcher(message).matches()) {
            orderController.handle(message);
        } else if (productMessage.matcher(message).matches()) {
            productController.handle(message);
        }
    }
}
