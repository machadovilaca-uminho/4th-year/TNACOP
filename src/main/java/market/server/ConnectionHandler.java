package market.server;

import market.Market;

import java.io.BufferedReader;
import java.io.IOException;

public class ConnectionHandler extends Thread {
    private final int connectionHashCode;
    private final BufferedReader in;

    public ConnectionHandler(int connectionHashCode, BufferedReader in) {
        this.connectionHashCode = connectionHashCode;
        this.in = in;
    }

    @Override
    public void run() {
        System.out.println("Listening on new connection");

        try {
            String userInput = in.readLine();
            System.out.println("User Message: " + userInput);
            String message = connectionHashCode + ";" + userInput;
            Market.inputParser.newMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
