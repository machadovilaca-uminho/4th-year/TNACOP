package market.repositories;

import org.hibernate.Session;
import org.hsqldb.lib.tar.DbBackupMain;
import org.hsqldb.lib.tar.TarMalformatException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Statement;

public class SnapshotDB {
    private static final String backup = "/tmp/tnacop-" + System.currentTimeMillis() + ".tar.gz";

    public static byte[] snap() {
        try {
            Path fileToDeletePath = Paths.get(backup);
            Files.deleteIfExists(fileToDeletePath);

            Session session = HibernateUtil.getSessionFactory().openSession();
            session.doWork(connection -> {
                Statement stmt = connection.createStatement();
                String sql = "BACKUP DATABASE TO '" + backup + "' BLOCKING";
                stmt.execute(sql);
            });
            session.close();

            return Files.readAllBytes(Paths.get(backup));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new byte[0];
    }

    public static void unsnap(byte[] log) {
        try {
            Files.write(Paths.get(backup), log);

            DbBackupMain.main(new String[]{
                    "--extract",
                    "--overwrite",
                    backup,
                    HibernateUtil.getFilePath(),
            });
        } catch (IOException | TarMalformatException e) {
            e.printStackTrace();
        }
    }
}
