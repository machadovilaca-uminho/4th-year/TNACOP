package market.repositories;

import market.models.Client;
import market.models.Order;
import market.models.Product;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.stream.Collectors;

public class OrderRepository {
    public OrderRepository() {
    }

    public List<Order> get() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Order> cq = cb.createQuery(Order.class);
        Root<Order> rootEntry = cq.from(Order.class);
        CriteriaQuery<Order> all = cq.select(rootEntry);

        List<Order> allOrders = session.createQuery(all).getResultList();

        session.close();

        return allOrders;
    }

    public long save(long clientId) {
        Order order = new Order();
        order.setCompleted(false);

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        order.setClient(session.get(Client.class, clientId));
        long id = (long) session.save(order);

        transaction.commit();
        session.close();

        return id;
    }

    public Order show(long orderId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Order order = session.get(Order.class, orderId);

        transaction.commit();
        session.close();

        return order;
    }

    public void addProduct(long orderId, long productId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Order order = session.get(Order.class, orderId);
        Product product = session.get(Product.class, productId);

        order.getProducts().add(product);
        session.saveOrUpdate(order);

        transaction.commit();
        session.close();
    }

    public void removeProduct(long orderId, long productId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Order order = session.get(Order.class, orderId);

        order.setProducts(
                order.getProducts()
                        .stream()
                        .filter(product -> product.getId() != productId)
                        .collect(Collectors.toSet())
        );

        session.saveOrUpdate(order);

        transaction.commit();
        session.close();
    }

    public boolean finish(long orderId) {
        boolean canFinish = false;
        Order order;

        int TMAX = 5;
        while (!canFinish && TMAX > 0) {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();

            order = session.get(Order.class, orderId);

            canFinish = order.getProducts()
                    .stream()
                    .noneMatch(p -> p.getStock() <= 0);

            if (canFinish) {
                order.getProducts().forEach(product -> {
                    product.setStock(product.getStock() - 1);
                });

                order.setCompleted(true);
                session.saveOrUpdate(order);
            }

            transaction.commit();
            session.close();

            TMAX--;
        }

        return canFinish;
    }
}
