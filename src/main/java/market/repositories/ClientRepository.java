package market.repositories;

import market.models.Client;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class ClientRepository {
    public ClientRepository() {
    }

    public List<Client> get() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Client> cq = cb.createQuery(Client.class);
        Root<Client> rootEntry = cq.from(Client.class);
        CriteriaQuery<Client> all = cq.select(rootEntry);

        List<Client> allClients = session.createQuery(all).getResultList();

        session.close();

        return allClients;
    }

    public long save(String name) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Client client = new Client();
        client.setName(name);
        long id = (long) session.save(client);

        transaction.commit();
        session.close();

        return id;
    }

    public Client show(long clientId) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Client client = session.get(Client.class, clientId);

        session.close();

        return client;
    }
}
