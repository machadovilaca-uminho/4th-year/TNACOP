package market.repositories;

import market.models.Client;
import market.models.Order;
import market.models.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.sql.Statement;

public class HibernateUtil {
    private static final String filePath = "/tmp/tnacop/" + System.currentTimeMillis();
    private static SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            if (sessionFactory == null) {
                Configuration configuration = new Configuration();

                configuration.setProperty("hibernate.connection.url", "jdbc:hsqldb:file:" + filePath + "/db;hsqldb.lock_file=false");
                configuration.configure(HibernateUtil.class.getResource("/hibernate.cfg.xml"));

                configuration.addAnnotatedClass(Client.class);
                configuration.addAnnotatedClass(Order.class);
                configuration.addAnnotatedClass(Product.class);

                StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
                serviceRegistryBuilder.applySettings(configuration.getProperties());
                ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            }

            return sessionFactory;
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return buildSessionFactory();
    }

    public static void shutdown() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.doWork(connection -> {
            Statement stmt = connection.createStatement();
            String sql = "SHUTDOWN";
            stmt.execute(sql);
        });
        session.close();

        sessionFactory.close();
        sessionFactory = null;
    }

    public static String getFilePath() {
        return filePath;
    }
}
