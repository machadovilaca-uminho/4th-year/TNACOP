package market.repositories;

import market.models.Product;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class ProductRepository {
    public ProductRepository() {
    }

    public List<Product> get() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Product> cq = cb.createQuery(Product.class);
        Root<Product> rootEntry = cq.from(Product.class);
        CriteriaQuery<Product> all = cq.select(rootEntry);

        List<Product> allProducts = session.createQuery(all).getResultList();

        session.close();

        return allProducts;
    }

    public Product show(long productId) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        Product product = session.get(Product.class, productId);

        session.close();

        return product;
    }

    public long save(String name, int price, int stock) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Product p = new Product();
        p.setName(name);
        p.setPrice(price);
        p.setStock(stock);

        long id = (long) session.save(p);

        transaction.commit();
        session.close();

        return id;
    }
}
