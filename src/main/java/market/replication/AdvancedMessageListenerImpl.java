package market.replication;

import market.server.Server;
import market.repositories.HibernateUtil;
import market.repositories.SnapshotDB;
import org.apache.commons.lang3.SerializationUtils;
import spread.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class AdvancedMessageListenerImpl implements AdvancedMessageListener {
    private static SpreadConnection connection;
    private static String myName;
    private static InputParser inputParser;
    private static HeartBeat heartBeat;

    private static String primary;
    private static ConcurrentLinkedQueue<String> primaryCandidates;
    private static SpreadGroup[] group = {};

    private static boolean initializing = true;

    private Server server = null;

    public static void setConnection(SpreadConnection connection) {
        AdvancedMessageListenerImpl.connection = connection;
    }

    public static void setMyName(String myName) {
        AdvancedMessageListenerImpl.myName = myName;
    }

    public static boolean amIPrimaryServer() {
        return primaryCandidates.size() == 0;
    }

    public static void setInputParser(InputParser inputParser) {
        AdvancedMessageListenerImpl.inputParser = inputParser;
    }

    public void membershipMessageReceived(SpreadMessage spreadMessage) {
        MembershipInfo mi = spreadMessage.getMembershipInfo();

        group = spreadMessage.getGroups();

        if (mi.isCausedByJoin()) {
            System.out.println("New member joined");
            newMember(mi);
        } else if (mi.isCausedByDisconnect() || mi.isCausedByLeave()) {
            System.out.println("A member left the group");
            faultyMember(mi);
        } else if (mi.isCausedByNetwork()) {
            System.out.println("Network issue occurred");
            handleNetworkIssue(mi);
        }

        if (heartBeat != null) {
            heartBeat.cancel();
        }

        heartBeat = new HeartBeat(this, connection, group, amIPrimaryServer());
    }

    public void regularMessageReceived(SpreadMessage spreadMessage) {
        byte[] data = spreadMessage.getData();
        int type = data[0];

        byte[] m = Arrays.copyOfRange(data, 1, data.length);

        if (type == 1 && spreadGroupIsMe(spreadMessage.getSender())) {
            initializing = false;
            primary = myName;

            if (server == null) {
                server = new Server(myName);
                server.start();
            }
        }

        switch (type) {
            case 1 -> {
                if (initializing) {
                    updateMyState(m);
                    initializing = false;
                }
            }
            case 2 -> inputParser.handleMessage(new String(m));
            case 3 -> {
                primary = new String(m);
                System.out.println(primary + " is now primary server");
            }
            case 4 -> primaryCandidates = SerializationUtils.deserialize(m);
            case 5 -> heartBeat.incrementBeats();
        }
    }

    public void replicate(String message) {
        if (amIPrimaryServer()) {
            byte[] repType = {2};
            byte[] repM = message.getBytes();

            System.out.println("replicating...");

            send(repType, repM, group);
        }
    }

    public void primaryIsDown() {
        List<String> currentMembers = new ArrayList<>();

        for (SpreadGroup member : group) {
            String memberName = member.toString().split("#")[1];

            if (!memberName.equals(primary)) {
                currentMembers.add(memberName);
            }
        }

        updateMembers(currentMembers);
    }


    /* ----- PRIVATE ----- */


    private void newMember(MembershipInfo mi) {
        if (primaryCandidates == null) {
            primaryCandidates = new ConcurrentLinkedQueue<>();
            for (SpreadGroup member : mi.getMembers()) {
                if (!spreadGroupIsMe(member)) {
                    primaryCandidates.add(getSpreadGroupName(member));
                }
            }
        }

        if (amIPrimaryServer()) {
            byte[] repM = SnapshotDB.snap();
            byte[] repType = {1};
            send(repType, repM, group);
        }
    }

    private void faultyMember(MembershipInfo mi) {
        List<String> currentMembers = new ArrayList<>();

        for (SpreadGroup member : mi.getMembers()) {
            String memberName = member.toString().split("#")[1];
            currentMembers.add(memberName);
        }

        updateMembers(currentMembers);
    }

    private void updateMembers(List<String> currentMembers) {
        primaryCandidates.forEach(c -> {
            if (!currentMembers.contains(c)) {
                primaryCandidates.remove(c);
            }
        });

        if (amIPrimaryServer()) {
            byte[] repType = {3};
            byte[] repM = myName.getBytes();
            send(repType, repM, group);

            if (server == null) {
                server = new Server(myName);
                server.start();
            }

            System.out.println("Taking over as Primary Server");
        }
    }

    private boolean spreadGroupIsMe(SpreadGroup spreadGroup) {
        String s = getSpreadGroupName(spreadGroup);
        return s.equals(myName);
    }

    private String getSpreadGroupName(SpreadGroup spreadGroup) {
        return spreadGroup.toString().split("#")[1];
    }

    private void send(byte[] repType, byte[] repM, SpreadGroup[] targets) {
        byte[] both = Arrays.copyOf(repType, repType.length + repM.length);
        System.arraycopy(repM, 0, both, repType.length, repM.length);

        SpreadMessage rep = new SpreadMessage();
        rep.setData(both);
        rep.setReliable();
        rep.addGroups(targets);
        try {
            connection.multicast(rep);
        } catch (SpreadException e) {
            e.printStackTrace();
        }
    }

    private void updateMyState(byte[] m) {
        HibernateUtil.shutdown();
        SnapshotDB.unsnap(m);
    }

    private void handleNetworkIssue(MembershipInfo mi) {
        SpreadGroup[] nodes = mi.getStayed();
        exitIfNotWithPrimary(nodes);

        if (!amIPrimaryServer()) {
            return;
        }

        ConcurrentLinkedQueue<String> primaryCandidates = new ConcurrentLinkedQueue<>();

        for (SpreadGroup node : nodes) {
            String sg = getSpreadGroupName(node);

            byte[] repType = {4};
            byte[] repM = SerializationUtils.serialize(primaryCandidates);
            send(repType, repM, new SpreadGroup[]{node});

            primaryCandidates.add(sg);
        }
    }

    private void exitIfNotWithPrimary(SpreadGroup[] nodes) {
        boolean hasPrimary = false;

        for (SpreadGroup node : nodes) {
            if (getSpreadGroupName(node).equals(primary)) {
                hasPrimary = true;
                break;
            }
        }

        if (!hasPrimary) {
            System.exit(101);
        }
    }
}
