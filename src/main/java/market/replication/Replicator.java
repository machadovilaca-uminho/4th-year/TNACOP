package market.replication;

import market.Market;
import spread.SpreadConnection;
import spread.SpreadException;
import spread.SpreadGroup;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Replicator {
    private AdvancedMessageListenerImpl advancedMessageListener;

    public void start(String serverId) throws UnknownHostException, SpreadException {
        SpreadConnection c = new SpreadConnection();
        c.connect(InetAddress.getByName("localhost"), 4803, serverId, false, true);
        SpreadGroup g = new SpreadGroup();
        g.join(c, "banks");

        AdvancedMessageListenerImpl.setConnection(c);
        AdvancedMessageListenerImpl.setMyName(serverId);
        AdvancedMessageListenerImpl.setInputParser(Market.inputParser);

        advancedMessageListener = new AdvancedMessageListenerImpl();
        c.add(advancedMessageListener);
    }

    public void replicate(String message) {
        advancedMessageListener.replicate(message);
    }
}
