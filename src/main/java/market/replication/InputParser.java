package market.replication;

public abstract class InputParser {
    private final Replicator replicator;

    public InputParser(Replicator replicator) {
        this.replicator = replicator;
    }

    public void newMessage(String message) {
        replicator.replicate(message);
    }

    public abstract void handleMessage(String message);
}
