package market.replication;

import spread.SpreadConnection;
import spread.SpreadException;
import spread.SpreadGroup;
import spread.SpreadMessage;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class HeartBeat extends Thread {
    private final AdvancedMessageListenerImpl spreadH;
    private final SpreadConnection connection;
    private final boolean primary;

    private SpreadGroup[] targets;
    private boolean running;
    private int beats;

    public HeartBeat(
            AdvancedMessageListenerImpl spreadH, SpreadConnection connection,
            SpreadGroup[] targets, boolean primary) {
        this.spreadH = spreadH;
        this.connection = connection;
        this.primary = primary;

        this.targets = targets;
        this.running = true;
        this.beats = 0;
    }

    public void setTargets(SpreadGroup[] targets) {
        this.targets = targets;
    }

    public void cancel() {
        this.running = false;
    }

    public void incrementBeats() {
        beats++;
    }

    private void send(byte[] repType) {
        SpreadMessage rep = new SpreadMessage();
        rep.setData(repType);
        rep.setReliable();
        rep.addGroups(targets);
        try {
            connection.multicast(rep);
        } catch (SpreadException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        if (primary) {
            primaryHeartBeat();
        } else {
            listenHeartBeats();
        }
    }

    private void primaryHeartBeat() {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(() -> {
            byte[] repType = {5};
            send(repType);
        }, 0, 1, TimeUnit.SECONDS);
    }

    private void listenHeartBeats() {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(() -> {
            if (beats == 0) {
                spreadH.primaryIsDown();
            } else {
                beats = 0;
            }
        }, 0, 5, TimeUnit.SECONDS);
    }
}
